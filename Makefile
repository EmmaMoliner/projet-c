#############################################################################
# Makefile for building: projet-c
# Generated by qmake (2.01a) (Qt 4.8.7) on: mar. janv. 7 10:14:54 2020
# Project:  projet-c.pro
# Template: app
# Command: /usr/lib/x86_64-linux-gnu/qt4/bin/qmake -o Makefile projet-c.pro
#############################################################################

####### Compiler, tools and options

CC            = gcc
CXX           = g++
DEFINES       = -DQT_NO_DEBUG -DQT_GUI_LIB -DQT_CORE_LIB -DQT_SHARED
CFLAGS        = -m64 -pipe -O2 -Wall -W -D_REENTRANT $(DEFINES)
CXXFLAGS      = -m64 -pipe -O2 -Wall -W -D_REENTRANT $(DEFINES)
INCPATH       = -I/usr/share/qt4/mkspecs/linux-g++-64 -I. -I/usr/include/qt4/QtCore -I/usr/include/qt4/QtGui -I/usr/include/qt4 -I. -I.
LINK          = g++
LFLAGS        = -m64 -Wl,-O1
LIBS          = $(SUBLIBS)  -L/usr/lib/x86_64-linux-gnu -lQtGui -lQtCore -lpthread 
AR            = ar cqs
RANLIB        = 
QMAKE         = /usr/lib/x86_64-linux-gnu/qt4/bin/qmake
TAR           = tar -cf
COMPRESS      = gzip -9f
COPY          = cp -f
SED           = sed
COPY_FILE     = $(COPY)
COPY_DIR      = $(COPY) -r
STRIP         = strip
INSTALL_FILE  = install -m 644 -p
INSTALL_DIR   = $(COPY_DIR)
INSTALL_PROGRAM = install -m 755 -p
DEL_FILE      = rm -f
SYMLINK       = ln -f -s
DEL_DIR       = rmdir
MOVE          = mv -f
CHK_DIR_EXISTS= test -d
MKDIR         = mkdir -p

####### Output directory

OBJECTS_DIR   = ./

####### Files

SOURCES       = authentification.cc \
		BD.cc \
		dessiner_systeme.cc \
		element.cc \
		entrainement.cc \
		exercice.cc \
		exercice1.cc \
		exercice2.cc \
		exercice3.cc \
		exercice4.cc \
		exercice5.cc \
		historique.cc \
		moteur.cc \
		pompe.cc \
		reservoir.cc \
		resolutions.cc \
		sauvegarde.cc \
		Simulateur.cc \
		sList.cc \
		solution.cc \
		systeme.cc \
		tableauDeBord.cc \
		vanne.cc \
		vanneTM.cc \
		vanneTT.cc moc_tableauDeBord.cpp
OBJECTS       = authentification.o \
		BD.o \
		dessiner_systeme.o \
		element.o \
		entrainement.o \
		exercice.o \
		exercice1.o \
		exercice2.o \
		exercice3.o \
		exercice4.o \
		exercice5.o \
		historique.o \
		moteur.o \
		pompe.o \
		reservoir.o \
		resolutions.o \
		sauvegarde.o \
		Simulateur.o \
		sList.o \
		solution.o \
		systeme.o \
		tableauDeBord.o \
		vanne.o \
		vanneTM.o \
		vanneTT.o \
		moc_tableauDeBord.o
DIST          = /usr/share/qt4/mkspecs/common/unix.conf \
		/usr/share/qt4/mkspecs/common/linux.conf \
		/usr/share/qt4/mkspecs/common/gcc-base.conf \
		/usr/share/qt4/mkspecs/common/gcc-base-unix.conf \
		/usr/share/qt4/mkspecs/common/g++-base.conf \
		/usr/share/qt4/mkspecs/common/g++-unix.conf \
		/usr/share/qt4/mkspecs/qconfig.pri \
		/usr/share/qt4/mkspecs/features/qt_functions.prf \
		/usr/share/qt4/mkspecs/features/qt_config.prf \
		/usr/share/qt4/mkspecs/features/exclusive_builds.prf \
		/usr/share/qt4/mkspecs/features/default_pre.prf \
		/usr/share/qt4/mkspecs/features/release.prf \
		/usr/share/qt4/mkspecs/features/default_post.prf \
		/usr/share/qt4/mkspecs/features/shared.prf \
		/usr/share/qt4/mkspecs/features/unix/gdb_dwarf_index.prf \
		/usr/share/qt4/mkspecs/features/warn_on.prf \
		/usr/share/qt4/mkspecs/features/qt.prf \
		/usr/share/qt4/mkspecs/features/unix/thread.prf \
		/usr/share/qt4/mkspecs/features/moc.prf \
		/usr/share/qt4/mkspecs/features/resources.prf \
		/usr/share/qt4/mkspecs/features/uic.prf \
		/usr/share/qt4/mkspecs/features/yacc.prf \
		/usr/share/qt4/mkspecs/features/lex.prf \
		/usr/share/qt4/mkspecs/features/include_source_dir.prf \
		projet-c.pro
QMAKE_TARGET  = projet-c
DESTDIR       = 
TARGET        = projet-c

first: all
####### Implicit rules

.SUFFIXES: .o .c .cpp .cc .cxx .C

.cpp.o:
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o "$@" "$<"

.cc.o:
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o "$@" "$<"

.cxx.o:
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o "$@" "$<"

.C.o:
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o "$@" "$<"

.c.o:
	$(CC) -c $(CFLAGS) $(INCPATH) -o "$@" "$<"

####### Build rules

all: Makefile $(TARGET)

$(TARGET):  $(OBJECTS)  
	$(LINK) $(LFLAGS) -o $(TARGET) $(OBJECTS) $(OBJCOMP) $(LIBS)

Makefile: projet-c.pro  /usr/share/qt4/mkspecs/linux-g++-64/qmake.conf /usr/share/qt4/mkspecs/common/unix.conf \
		/usr/share/qt4/mkspecs/common/linux.conf \
		/usr/share/qt4/mkspecs/common/gcc-base.conf \
		/usr/share/qt4/mkspecs/common/gcc-base-unix.conf \
		/usr/share/qt4/mkspecs/common/g++-base.conf \
		/usr/share/qt4/mkspecs/common/g++-unix.conf \
		/usr/share/qt4/mkspecs/qconfig.pri \
		/usr/share/qt4/mkspecs/features/qt_functions.prf \
		/usr/share/qt4/mkspecs/features/qt_config.prf \
		/usr/share/qt4/mkspecs/features/exclusive_builds.prf \
		/usr/share/qt4/mkspecs/features/default_pre.prf \
		/usr/share/qt4/mkspecs/features/release.prf \
		/usr/share/qt4/mkspecs/features/default_post.prf \
		/usr/share/qt4/mkspecs/features/shared.prf \
		/usr/share/qt4/mkspecs/features/unix/gdb_dwarf_index.prf \
		/usr/share/qt4/mkspecs/features/warn_on.prf \
		/usr/share/qt4/mkspecs/features/qt.prf \
		/usr/share/qt4/mkspecs/features/unix/thread.prf \
		/usr/share/qt4/mkspecs/features/moc.prf \
		/usr/share/qt4/mkspecs/features/resources.prf \
		/usr/share/qt4/mkspecs/features/uic.prf \
		/usr/share/qt4/mkspecs/features/yacc.prf \
		/usr/share/qt4/mkspecs/features/lex.prf \
		/usr/share/qt4/mkspecs/features/include_source_dir.prf \
		/usr/lib/x86_64-linux-gnu/libQtGui.prl \
		/usr/lib/x86_64-linux-gnu/libQtCore.prl
	$(QMAKE) -o Makefile projet-c.pro
/usr/share/qt4/mkspecs/common/unix.conf:
/usr/share/qt4/mkspecs/common/linux.conf:
/usr/share/qt4/mkspecs/common/gcc-base.conf:
/usr/share/qt4/mkspecs/common/gcc-base-unix.conf:
/usr/share/qt4/mkspecs/common/g++-base.conf:
/usr/share/qt4/mkspecs/common/g++-unix.conf:
/usr/share/qt4/mkspecs/qconfig.pri:
/usr/share/qt4/mkspecs/features/qt_functions.prf:
/usr/share/qt4/mkspecs/features/qt_config.prf:
/usr/share/qt4/mkspecs/features/exclusive_builds.prf:
/usr/share/qt4/mkspecs/features/default_pre.prf:
/usr/share/qt4/mkspecs/features/release.prf:
/usr/share/qt4/mkspecs/features/default_post.prf:
/usr/share/qt4/mkspecs/features/shared.prf:
/usr/share/qt4/mkspecs/features/unix/gdb_dwarf_index.prf:
/usr/share/qt4/mkspecs/features/warn_on.prf:
/usr/share/qt4/mkspecs/features/qt.prf:
/usr/share/qt4/mkspecs/features/unix/thread.prf:
/usr/share/qt4/mkspecs/features/moc.prf:
/usr/share/qt4/mkspecs/features/resources.prf:
/usr/share/qt4/mkspecs/features/uic.prf:
/usr/share/qt4/mkspecs/features/yacc.prf:
/usr/share/qt4/mkspecs/features/lex.prf:
/usr/share/qt4/mkspecs/features/include_source_dir.prf:
/usr/lib/x86_64-linux-gnu/libQtGui.prl:
/usr/lib/x86_64-linux-gnu/libQtCore.prl:
qmake:  FORCE
	@$(QMAKE) -o Makefile projet-c.pro

dist: 
	@$(CHK_DIR_EXISTS) .tmp/projet-c1.0.0 || $(MKDIR) .tmp/projet-c1.0.0 
	$(COPY_FILE) --parents $(SOURCES) $(DIST) .tmp/projet-c1.0.0/ && $(COPY_FILE) --parents authentification.hh BD.hh dessiner_systeme.hh element.hh entrainement.hh exercice.hh exercice1.hh exercice2.hh exercice3.hh exercice4.hh exercice5.hh historique.hh moteur.hh pompe.hh reservoir.hh resolutions.hh sauvegarde.hh sList.hh solution.hh systeme.hh tableauDeBord.hh vanne.hh vanneTM.hh vanneTT.hh .tmp/projet-c1.0.0/ && $(COPY_FILE) --parents authentification.cc BD.cc dessiner_systeme.cc element.cc entrainement.cc exercice.cc exercice1.cc exercice2.cc exercice3.cc exercice4.cc exercice5.cc historique.cc moteur.cc pompe.cc reservoir.cc resolutions.cc sauvegarde.cc Simulateur.cc sList.cc solution.cc systeme.cc tableauDeBord.cc vanne.cc vanneTM.cc vanneTT.cc .tmp/projet-c1.0.0/ && (cd `dirname .tmp/projet-c1.0.0` && $(TAR) projet-c1.0.0.tar projet-c1.0.0 && $(COMPRESS) projet-c1.0.0.tar) && $(MOVE) `dirname .tmp/projet-c1.0.0`/projet-c1.0.0.tar.gz . && $(DEL_FILE) -r .tmp/projet-c1.0.0


clean:compiler_clean 
	-$(DEL_FILE) $(OBJECTS)
	-$(DEL_FILE) *~ core *.core


####### Sub-libraries

distclean: clean
	-$(DEL_FILE) $(TARGET) 
	-$(DEL_FILE) Makefile


check: first

mocclean: compiler_moc_header_clean compiler_moc_source_clean

mocables: compiler_moc_header_make_all compiler_moc_source_make_all

compiler_moc_header_make_all: moc_tableauDeBord.cpp
compiler_moc_header_clean:
	-$(DEL_FILE) moc_tableauDeBord.cpp
moc_tableauDeBord.cpp: systeme.hh \
		moteur.hh \
		reservoir.hh \
		pompe.hh \
		element.hh \
		vanneTT.hh \
		vanne.hh \
		vanneTM.hh \
		dessiner_systeme.hh \
		sauvegarde.hh \
		tableauDeBord.hh
	/usr/lib/x86_64-linux-gnu/qt4/bin/moc $(DEFINES) $(INCPATH) tableauDeBord.hh -o moc_tableauDeBord.cpp

compiler_rcc_make_all:
compiler_rcc_clean:
compiler_image_collection_make_all: qmake_image_collection.cpp
compiler_image_collection_clean:
	-$(DEL_FILE) qmake_image_collection.cpp
compiler_moc_source_make_all:
compiler_moc_source_clean:
compiler_uic_make_all:
compiler_uic_clean:
compiler_yacc_decl_make_all:
compiler_yacc_decl_clean:
compiler_yacc_impl_make_all:
compiler_yacc_impl_clean:
compiler_lex_make_all:
compiler_lex_clean:
compiler_clean: compiler_moc_header_clean 

####### Compile

authentification.o: authentification.cc authentification.hh \
		BD.hh
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o authentification.o authentification.cc

BD.o: BD.cc BD.hh
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o BD.o BD.cc

dessiner_systeme.o: dessiner_systeme.cc dessiner_systeme.hh
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o dessiner_systeme.o dessiner_systeme.cc

element.o: element.cc element.hh
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o element.o element.cc

entrainement.o: entrainement.cc entrainement.hh \
		systeme.hh \
		moteur.hh \
		reservoir.hh \
		pompe.hh \
		element.hh \
		vanneTT.hh \
		vanne.hh \
		vanneTM.hh \
		dessiner_systeme.hh
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o entrainement.o entrainement.cc

exercice.o: exercice.cc exercice.hh \
		systeme.hh \
		moteur.hh \
		reservoir.hh \
		pompe.hh \
		element.hh \
		vanneTT.hh \
		vanne.hh \
		vanneTM.hh \
		dessiner_systeme.hh \
		sList.hh \
		solution.hh \
		sauvegarde.hh
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o exercice.o exercice.cc

exercice1.o: exercice1.cc exercice1.hh \
		exercice.hh \
		systeme.hh \
		moteur.hh \
		reservoir.hh \
		pompe.hh \
		element.hh \
		vanneTT.hh \
		vanne.hh \
		vanneTM.hh \
		dessiner_systeme.hh \
		sList.hh \
		solution.hh \
		sauvegarde.hh
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o exercice1.o exercice1.cc

exercice2.o: exercice2.cc exercice2.hh \
		exercice.hh \
		systeme.hh \
		moteur.hh \
		reservoir.hh \
		pompe.hh \
		element.hh \
		vanneTT.hh \
		vanne.hh \
		vanneTM.hh \
		dessiner_systeme.hh \
		sList.hh \
		solution.hh \
		sauvegarde.hh
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o exercice2.o exercice2.cc

exercice3.o: exercice3.cc exercice3.hh \
		exercice.hh \
		systeme.hh \
		moteur.hh \
		reservoir.hh \
		pompe.hh \
		element.hh \
		vanneTT.hh \
		vanne.hh \
		vanneTM.hh \
		dessiner_systeme.hh \
		sList.hh \
		solution.hh \
		sauvegarde.hh
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o exercice3.o exercice3.cc

exercice4.o: exercice4.cc exercice4.hh \
		exercice.hh \
		systeme.hh \
		moteur.hh \
		reservoir.hh \
		pompe.hh \
		element.hh \
		vanneTT.hh \
		vanne.hh \
		vanneTM.hh \
		dessiner_systeme.hh \
		sList.hh \
		solution.hh \
		sauvegarde.hh
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o exercice4.o exercice4.cc

exercice5.o: exercice5.cc exercice5.hh \
		exercice.hh \
		systeme.hh \
		moteur.hh \
		reservoir.hh \
		pompe.hh \
		element.hh \
		vanneTT.hh \
		vanne.hh \
		vanneTM.hh \
		dessiner_systeme.hh \
		sList.hh \
		solution.hh \
		sauvegarde.hh
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o exercice5.o exercice5.cc

historique.o: historique.cc historique.hh \
		sauvegarde.hh \
		sList.hh \
		solution.hh
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o historique.o historique.cc

moteur.o: moteur.cc moteur.hh \
		reservoir.hh \
		pompe.hh \
		element.hh
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o moteur.o moteur.cc

pompe.o: pompe.cc pompe.hh \
		element.hh
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o pompe.o pompe.cc

reservoir.o: reservoir.cc reservoir.hh \
		pompe.hh \
		element.hh
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o reservoir.o reservoir.cc

resolutions.o: resolutions.cc resolutions.hh \
		solution.hh \
		sauvegarde.hh \
		sList.hh
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o resolutions.o resolutions.cc

sauvegarde.o: sauvegarde.cc sauvegarde.hh
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o sauvegarde.o sauvegarde.cc

Simulateur.o: Simulateur.cc systeme.hh \
		moteur.hh \
		reservoir.hh \
		pompe.hh \
		element.hh \
		vanneTT.hh \
		vanne.hh \
		vanneTM.hh \
		dessiner_systeme.hh \
		tableauDeBord.hh \
		sauvegarde.hh \
		exercice1.hh \
		exercice.hh \
		sList.hh \
		solution.hh \
		exercice2.hh \
		exercice3.hh \
		exercice4.hh \
		exercice5.hh \
		historique.hh \
		BD.hh \
		authentification.hh \
		entrainement.hh \
		resolutions.hh
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o Simulateur.o Simulateur.cc

sList.o: sList.cc sList.hh \
		solution.hh \
		sauvegarde.hh
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o sList.o sList.cc

solution.o: solution.cc solution.hh \
		sauvegarde.hh
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o solution.o solution.cc

systeme.o: systeme.cc systeme.hh \
		moteur.hh \
		reservoir.hh \
		pompe.hh \
		element.hh \
		vanneTT.hh \
		vanne.hh \
		vanneTM.hh
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o systeme.o systeme.cc

tableauDeBord.o: tableauDeBord.cc tableauDeBord.hh \
		systeme.hh \
		moteur.hh \
		reservoir.hh \
		pompe.hh \
		element.hh \
		vanneTT.hh \
		vanne.hh \
		vanneTM.hh \
		dessiner_systeme.hh \
		sauvegarde.hh
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o tableauDeBord.o tableauDeBord.cc

vanne.o: vanne.cc vanne.hh \
		element.hh
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o vanne.o vanne.cc

vanneTM.o: vanneTM.cc vanneTM.hh \
		vanne.hh \
		element.hh \
		reservoir.hh \
		pompe.hh \
		moteur.hh
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o vanneTM.o vanneTM.cc

vanneTT.o: vanneTT.cc vanneTT.hh \
		vanne.hh \
		element.hh \
		reservoir.hh \
		pompe.hh
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o vanneTT.o vanneTT.cc

moc_tableauDeBord.o: moc_tableauDeBord.cpp 
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o moc_tableauDeBord.o moc_tableauDeBord.cpp

####### Install

install:   FORCE

uninstall:   FORCE

FORCE:

