#include "ZoneDessin.hh"

bool ZoneDessin::on_draw(const RefPtr<Context> &cr){

  /*Récupération de la taille de la zone de dessin*/
  Allocation zone = get_allocation();
  int width = zone.get_width();
  int height = zone.get_height();

  /*Calcul du centre de la zone*/
  int centreX = width/2;
  int centreY = height/2;

  cr->set_font_size(15);

  /*Création du rectangle orange représentant Tank1*/
  cr->set_source_rgb(1,0.5,0);                                                  /*Initialisation de la couleur*/
  cr->rectangle(centreX-(centreX-25),centreY-(centreY-25),150,200);             /*Création du rectangle*/
  cr->fill();                                                                   /*Rectangle plein de la couleur intialisée en dernier*/
  cr->set_source_rgb(0.1,0.1,0.1);
  cr->move_to(centreX-(centreX-25)+60,centreY-(centreY-25)+100);
  cr->show_text("Tank1");

  /*Création du rectangle vert représentant Tank2*/
  cr->set_source_rgb(0,0.5,0);
  cr->rectangle(centreX-75,centreY-(centreY-25),150,200);
  cr->fill();
  cr->set_source_rgb(0.1,0.1,0.1);
  cr->move_to(centreX-15,centreY-(centreY-25)+100);
  cr->show_text("Tank2");

  /*Création du rectangle jaune représentant Tank3*/
  cr->set_source_rgb(1,0.8,0);
  cr->rectangle(centreX+(centreX-175),centreY-(centreY-25),150,200);
  cr->fill();
  cr->set_source_rgb(0.1,0.1,0.1);
  cr->move_to(centreX+(centreX-175)+60,centreY-(centreY-25)+100);
  cr->show_text("Tank3");

  /*Création de 3 rectangles gris représentant les moteurs*/
  cr->set_source_rgb(0.5,0.5,0.5);
  cr->rectangle(centreX-(centreX-75),centreY+(centreY/2),50,150);
  cr->rectangle(centreX-25,centreY+(centreY/2),50,150);
  cr->rectangle(centreX+centreX/2+50,centreY+(centreY/2),50,150);
  cr->fill();
  cr->set_source_rgb(0.1,0.1,0.1);
  cr->move_to(centreX-(centreX-75)+18,centreY+(centreY/2)+75);                  /*Placement au point indiqué*/
  cr->show_text("M1");
  cr->move_to(centreX-8,centreY+(centreY/2)+75);                                /*Placement au point indiqué*/
  cr->show_text("M2");
  cr->move_to(centreX+centreX/2+68,centreY+(centreY/2)+75);                     /*Placement au point indiqué*/
  cr->show_text("M3");

  /*Création des vannes*/

  /*Vanne Tank1/Tank2*/
  cr->arc(centreX-125,centreY-(centreY-25)+100,20.0,0.0,2.0*M_PI);
  cr->fill();
  cr->move_to(centreX-135,centreY-(centreY-25)+75);                             /*Placement au point indiqué*/
  cr->show_text("VT12");

  /*Vanne Tank2/Tank3*/
  cr->arc(centreX+125,centreY-(centreY-25)+100,20.0,0.0,2.0*M_PI);
  cr->fill();
  cr->move_to(centreX+115,centreY-(centreY-25)+75);                             /*Placement au point indiqué*/
  cr->show_text("VT23");

  /*Vanne Tank1/M3*/
  cr->arc(centreX+125,centreY-50,20.0,0.0,2.0*M_PI);
  cr->fill();
  cr->move_to(centreX+115,centreY-75);                                          /*Placement au point indiqué*/
  cr->show_text("V13");

  /*Vanne Tank1/M2*/
  cr->arc(centreX-125,centreY+130,20.0,0.0,2.0*M_PI);
  cr->fill();
  cr->move_to(centreX-135,centreY+105);                                          /*Placement au point indiqué*/
  cr->show_text("V12");

  /*Vanne Tank3/M2*/
  cr->arc(centreX+125,centreY+50,20.0,0.0,2.0*M_PI);
  cr->fill();
  cr->move_to(centreX+115,centreY+25);                                          /*Placement au point indiqué*/
  cr->show_text("V32");

  /*Pompes de Tank1*/

  /*Pompe primaire*/
  cr->set_source_rgb(0.4,0.8,0);
  cr->arc(centreX-(centreX-25)+60,centreY-(centreY-25)+160,25.0,0.0,2.0*M_PI);
  cr->fill();
  cr->set_source_rgb(1,1,1);
  cr->move_to(centreX-(centreX-25)+50,centreY-(centreY-25)+160);                                          /*Placement au point indiqué*/
  cr->show_text("p11");

  /*Pompe secondaire*/
  cr->set_source_rgb(0.1,0.1,0.1);
  cr->arc(centreX-(centreX-25)+115,centreY-(centreY-25)+160,25.0,0.0,2.0*M_PI);
  cr->fill();
  cr->set_source_rgb(1,1,1);
  cr->move_to(centreX-(centreX-25)+105,centreY-(centreY-25)+160);                                          /*Placement au point indiqué*/
  cr->show_text("p12");

  /*Pompes de Tank2*/

  /*Pompe primaire*/
  cr->set_source_rgb(0.4,0.8,0);
  cr->arc(centreX-15,centreY-(centreY-25)+160,25.0,0.0,2.0*M_PI);
  cr->fill();
  cr->set_source_rgb(1,1,1);
  cr->move_to(centreX-25,centreY-(centreY-25)+160);                                          /*Placement au point indiqué*/
  cr->show_text("p11");

  /*Pompe secondaire*/
  cr->set_source_rgb(0.1,0.1,0.1);
  cr->arc(centreX+40,centreY-(centreY-25)+160,25.0,0.0,2.0*M_PI);
  cr->fill();
  cr->set_source_rgb(1,1,1);
  cr->move_to(centreX+30,centreY-(centreY-25)+160);                                          /*Placement au point indiqué*/
  cr->show_text("p12");

  /*Pompes de Tank3*/

  /*Pompe primaire*/
  cr->set_source_rgb(0.4,0.8,0);
  cr->arc(centreX+235,centreY-(centreY-25)+160,25.0,0.0,2.0*M_PI);
  cr->fill();
  cr->set_source_rgb(1,1,1);
  cr->move_to(centreX+225,centreY-(centreY-25)+160);                                          /*Placement au point indiqué*/
  cr->show_text("p12");

  /*Pompe secondaire*/
  cr->set_source_rgb(0.1,0.1,0.1);
  cr->arc(centreX+290,centreY-(centreY-25)+160,25.0,0.0,2.0*M_PI);
  cr->fill();
  cr->set_source_rgb(1,1,1);
  cr->move_to(centreX+280,centreY-(centreY-25)+160);                                          /*Placement au point indiqué*/
  cr->show_text("p12");

  cr->set_source_rgb(0.1,0.1,0.1);

  /*Rectangle représentant l'emplacement des pompes de Tank1*/
  cr->move_to(centreX-(centreX-25)+150,centreY-(centreY-25)+125);
  cr->line_to(centreX-(centreX-25)+150,centreY-(centreY-25)+200);

  cr->move_to(centreX-(centreX-25)+25,centreY-(centreY-25)+125);
  cr->line_to(centreX-(centreX-25)+150,centreY-(centreY-25)+125);

  cr->move_to(centreX-(centreX-25)+25,centreY-(centreY-25)+125);
  cr->line_to(centreX-(centreX-25)+25,centreY-(centreY-25)+200);

  cr->move_to(centreX-(centreX-25)+25,centreY-(centreY-25)+200);
  cr->line_to(centreX-(centreX-25)+150,centreY-(centreY-25)+200);

  /*Emplacement des pompes de Tank2*/
  cr->move_to(centreX+75,centreY-(centreY-25)+125);
  cr->line_to(centreX+75,centreY-(centreY-25)+200);

  cr->move_to(centreX-50,centreY-(centreY-25)+125);
  cr->line_to(centreX-50,centreY-(centreY-25)+200);

  cr->move_to(centreX-50,centreY-(centreY-25)+125);
  cr->line_to(centreX+75,centreY-(centreY-25)+125);

  cr->move_to(centreX-50,centreY-(centreY-25)+200);
  cr->line_to(centreX+75,centreY-(centreY-25)+200);

  /*Emplacement des pompes de Tank3*/
  cr->move_to(centreX+200,centreY-(centreY-25)+125);
  cr->line_to(centreX+200,centreY-(centreY-25)+200);

  cr->move_to(centreX+325,centreY-(centreY-25)+125);
  cr->line_to(centreX+325,centreY-(centreY-25)+200);

  cr->move_to(centreX+200,centreY-(centreY-25)+125);
  cr->line_to(centreX+325,centreY-(centreY-25)+125);

  cr->move_to(centreX+200,centreY-(centreY-25)+200);
  cr->line_to(centreX+325,centreY-(centreY-25)+200);

  /*Création de ligne représentant les tuyaux*/
  cr->set_line_width(1.5);                                                      /*Initialisation de l'épaisseur du trait*/

  /*Liaison Tank1/Tank2*/
  cr->move_to(centreX-75,centreY-(centreY-25)+100);                             /*Déplacement au point de départ de la ligne*/
  cr->line_to(centreX-(centreX-25)+150,centreY-(centreY-25)+100);               /*Tracement de la ligne jusqu'au point d'arrivée*/

  /*Liaison Tank2/Tank3*/
  cr->move_to(centreX+(centreX-175),centreY-(centreY-25)+100);
  cr->line_to(centreX+75,centreY-(centreY-25)+100);

  /*Liaison Tank2/M2*/
  cr->move_to(centreX,centreY-(centreY-25)+200);
  cr->line_to(centreX,centreY+(centreY/2));

  /*Liaison Tank1/M1*/
  cr->move_to(centreX-(centreX-25)+75,centreY+75);
  cr->line_to(centreX-(centreX-25)+75,centreY+(centreY/2));
  cr->move_to(centreX-(centreX-25)+75,centreY+75);
  cr->line_to(centreX-(centreX-25)+100,centreY+75);

  /*Liaison Tank3/M3*/
  cr->move_to(centreX+(centreX-175)+50,centreY-(centreY-25)+200);
  cr->line_to(centreX+(centreX-175)+50,centreY+20);
  cr->move_to(centreX+(centreX-175)+50,centreY+20);
  cr->line_to(centreX+(centreX-175)+75,centreY+20);

  /*Liaison Tank1/M2*/
  cr->move_to(centreX,centreY+130);
  cr->line_to(centreX-(centreX-25)+100,centreY+130);
  cr->move_to(centreX-(centreX-25)+100,centreY-(centreY-25)+200);
  cr->line_to(centreX-(centreX-25)+100,centreY+130);

  /*Liaison Tank1/M3*/
  cr->move_to(centreX-(centreX-25)+100,centreY-50);
  cr->line_to(centreX+(centreX-100),centreY-50);
  cr->move_to(centreX+(centreX-175)+75,centreY-50);
  cr->line_to(centreX+(centreX-175)+75,centreY+(centreY/2));

  /*Liaison Tank3/M2*/
  cr->move_to(centreX+(centreX-175)+50,centreY+20);
  cr->line_to(centreX+(centreX-175)+50,centreY+50);
  cr->move_to(centreX+(centreX-175)+50,centreY+50);
  cr->line_to(centreX,centreY+50);

  cr->stroke();



  return true;

}
