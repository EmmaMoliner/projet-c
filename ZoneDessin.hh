#ifndef _ZONEDESSIN_HH_
#define _ZONEDESSIN_HH_

#include <gtkmm/drawingarea.h>

using namespace Gtk;
using namespace Cairo;

class ZoneDessin : public DrawingArea{

public:
  bool on_draw(const RefPtr<Context> &cr) override;
};
#endif
