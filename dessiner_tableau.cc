#include "dessiner_tableau.hh"

dessiner_tableau::dessiner_tableau(systeme& sys,dessiner_systeme& draw_sys,sauvegarde& sauv)
{
  this->draw_sys = &draw_sys;
  this->sys = &sys;
  this->sauv = &sauv;

  vt12 = new QPushButton("VT12");
  vt23 = new QPushButton("VT23");
  p12 = new QPushButton("P12");
  p22 = new QPushButton("P22");
  p32 = new QPushButton("P32");
  v12 = new QPushButton("V12");
  v13 = new QPushButton("V13");
  v23 = new QPushButton("V23");
  quit = new QPushButton("Valider");
  layout = new QGridLayout;

  layout->addWidget(vt12,0,0);
  layout->addWidget(vt23,0,1);
  layout->addWidget(p12,1,0);
  layout->addWidget(p22,1,1);
  layout->addWidget(p32,1,2);
  layout->addWidget(v12,2,0);
  layout->addWidget(v13,2,1);
  layout->addWidget(v23,2,2);
  layout->addWidget(quit,3,1);

  setLayout(layout);

  QWidget::connect(p12,SIGNAL(clicked()),this,SLOT(appuie_p12()));
  QWidget::connect(p22,SIGNAL(clicked()),this,SLOT(appuie_p22()));
  QWidget::connect(p32,SIGNAL(clicked()),this,SLOT(appuie_p32()));
  QWidget::connect(vt12,SIGNAL(clicked()),this,SLOT(appuie_vt12()));
  QWidget::connect(vt23,SIGNAL(clicked()),this,SLOT(appuie_vt23()));
  QWidget::connect(v12,SIGNAL(clicked()),this,SLOT(appuie_v12()));
  QWidget::connect(v13,SIGNAL(clicked()),this,SLOT(appuie_v13()));
  QWidget::connect(v23,SIGNAL(clicked()),this,SLOT(appuie_v23()));
  QWidget::connect(quit,SIGNAL(clicked()),this,SLOT(close()));

}

dessiner_tableau::~dessiner_tableau()
{
  delete vt12;
  delete vt23;
  delete p12;
  delete p22;
  delete p32;
  delete v12;
  delete v13;
  delete v23;
  delete quit;
  delete layout;
}

void dessiner_tableau::appuie_p12()
{

  if(sys->getPompeP12().getEtat()==0)
  {
    sys->getPompeP12().setEtat(1);
    draw_sys->ouvrir_P12();
  }
  else if(sys->getPompeP12().getEtat()==1)
  {
    sys->getPompeP12().setEtat(0);
    draw_sys->fermer_P12();
  }
  sauv->add(sys->getPompeP12().getNom(),sys->getPompeP12().getEtat());
  cout << *sys<<endl;
  cout << *sauv<<endl;
}

void dessiner_tableau::appuie_p22()
{
  if(sys->getPompeP22().getEtat()==0)
  {
    sys->getPompeP22().setEtat(1);
    draw_sys->ouvrir_P22();
  }
  else if(sys->getPompeP22().getEtat()==1)
  {
    sys->getPompeP22().setEtat(0);
    draw_sys->fermer_P22();
  }

  sauv->add(sys->getPompeP22().getNom(),sys->getPompeP22().getEtat());
  cout << *sys<<endl;
  cout << *sauv<<endl;
}

void dessiner_tableau::appuie_p32()
{
  if(sys->getPompeP32().getEtat()==0)
  {
    sys->getPompeP32().setEtat(1);
    draw_sys->ouvrir_P32();
  }
  else if(sys->getPompeP32().getEtat()==1)
  {
    sys->getPompeP32().setEtat(0);
    draw_sys->fermer_P32();
  }

  sauv->add(sys->getPompeP32().getNom(),sys->getPompeP32().getEtat());
  cout << *sys<<endl;
  cout << *sauv<<endl;
}

void dessiner_tableau::appuie_vt12()
{
  if(sys->getVanneVT12().getEtat()==0)
  {
    sys->getVanneVT12().setEtat(1);
    draw_sys->ouvrir_VT12();
  }
  else if(sys->getVanneVT12().getEtat()==1)
  {
    sys->getVanneVT12().setEtat(0);
    draw_sys->fermer_VT12();
  }

  sauv->add(sys->getVanneVT12().getNom(),sys->getVanneVT12().getEtat());
  cout << *sys<<endl;
  cout << *sauv<<endl;
}

void dessiner_tableau::appuie_vt23()
{
  if(sys->getVanneVT23().getEtat()==0)
  {
    sys->getVanneVT23().setEtat(1);
    draw_sys->ouvrir_VT23();
  }
  else if(sys->getVanneVT23().getEtat()==1)
  {
    sys->getVanneVT23().setEtat(0);
    draw_sys->fermer_VT23();
  }

  sauv->add(sys->getVanneVT23().getNom(),sys->getVanneVT23().getEtat());
  cout << *sys<<endl;
  cout << *sauv<<endl;
}

void dessiner_tableau::appuie_v12()
{
  if(sys->getVanneV12().getEtat()==0)
  {
    sys->getVanneV12().setEtat(1);
    draw_sys->ouvrir_V12();
  }
  else if(sys->getVanneV12().getEtat()==1)
  {
    sys->getVanneV12().setEtat(0);
    draw_sys->fermer_V12();
  }

  sauv->add(sys->getVanneV12().getNom(),sys->getVanneV12().getEtat());
  cout << *sys<<endl;
  cout << *sauv<<endl;
}

void dessiner_tableau::appuie_v13()
{
  if(sys->getVanneV13().getEtat()==0)
  {
    sys->getVanneV13().setEtat(1);
    draw_sys->ouvrir_V13();
    cout << *sys <<endl;
  }
  else if(sys->getVanneV13().getEtat()==1)
  {
    sys->getVanneV13().setEtat(0);
    draw_sys->fermer_V13();
    cout << *sys<<endl;
  }

  sauv->add(sys->getVanneV13().getNom(),sys->getVanneV13().getEtat());
  cout << *sys<<endl;
  cout << *sauv<<endl;
}

void dessiner_tableau::appuie_v23()
{
  if(sys->getVanneV23().getEtat()==0)
  {
    sys->getVanneV23().setEtat(1);
    draw_sys->ouvrir_V23();
  }
  else if(sys->getVanneV23().getEtat()==1)
  {
    sys->getVanneV23().setEtat(0);
    draw_sys->fermer_V23();
  }

  sauv->add(sys->getVanneV23().getNom(),sys->getVanneV23().getEtat());
  cout << *sys<<endl;
  cout << *sauv<<endl;
}
