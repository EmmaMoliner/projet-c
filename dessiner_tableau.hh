#ifndef _DESSINER_TABLEAU_HH_
#define _DESSINER_TABLEAU_HH_

#include <QtGui/QPushButton>
#include <QtGui/QGridLayout>
#include "systeme.hh"
#include "dessiner_systeme.hh"
#include "sauvegarde.hh"

class dessiner_tableau : public QWidget{
Q_OBJECT

private:
  QPushButton *vt12;
  QPushButton *vt23;
  QPushButton *p12;
  QPushButton *p22;
  QPushButton *p32;
  QPushButton *v12;
  QPushButton *v13;
  QPushButton *v23;
  QPushButton *quit;
  QGridLayout * layout;
  systeme * sys;
  dessiner_systeme * draw_sys;
  sauvegarde * sauv;


public slots:
  void appuie_p12();
  void appuie_p22();
  void appuie_p32();
  void appuie_vt12();
  void appuie_vt23();
  void appuie_v12();
  void appuie_v13();
  void appuie_v23();

public:
  dessiner_tableau(systeme& sys,dessiner_systeme& draw_sys,sauvegarde& sauv);
  ~dessiner_tableau();
};

#endif
