#ifndef _ELEMENT_H
#define _ELEMENT_H

#include <string>
#include <iostream>
#include <stdexcept>

using namespace std;

class element{

private:
  int etat;
  string nom;

public:
  element();
  element(const int state,const string name);
  element(element & e);
  ~element();
  int getEtat();
  string getNom();
  void setEtat(const int state);
  void setNom(const string name);
};

#endif
