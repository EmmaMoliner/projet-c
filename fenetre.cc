#include "fenetre.hh"

fenetre::fenetre(){
  set_title("");
  resize(400,400);
}

fenetre::fenetre(const string titre,const int longueur,const int largeur)
{
    set_title(titre);
    resize(longueur,largeur);
}

fenetre::~fenetre(){};
