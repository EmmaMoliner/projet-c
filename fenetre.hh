#ifndef _FENETRE_HH
#define _FENETRE_HH

#include <gtkmm/window.h>
#include <cstring>

using namespace Gtk;
using namespace std;

class fenetre:public Window{

public:
    fenetre();
    fenetre(const string titre,const int longueur,const int largeur);
    ~fenetre();

};

#endif
