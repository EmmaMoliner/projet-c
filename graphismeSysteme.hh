#ifndef _GRAPHISMESYSTEME_HH_
#define _GRAPHISMESYSTEME_HH_

#include <gtkmm/drawingarea.h>

using namespace Cairo;
using namespace Gtk;

class graphismeSysteme:public DrawingArea{

private:
  int height;
  int width;

public:
  bool on_draw(const Cairo::RefPtr<Cairo::Context>& cr) override;
  bool modifier_systeme();
};

#endif
