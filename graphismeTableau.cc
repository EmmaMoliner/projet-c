#include "graphismeTableau.hh"

graphismeTableau::graphismeTableau():boite(true,BUTTONBOX_SPREAD),P12("P12"),P22("P22"),P32("P32"),VT12("VT12"),VT23("VT23"),V12("V12"),V13("V13"),V32("V32")
{
  P12.set_can_focus(false);
  P22.set_can_focus(false);
  P32.set_can_focus(false);
  VT12.set_can_focus(false);
  VT23.set_can_focus(false);
  V12.set_can_focus(false);
  V13.set_can_focus(false);
  V32.set_can_focus(false);

  boite.pack_start(P12);
  boite.pack_start(P22);
  boite.pack_start(P32);
  boite.pack_start(VT12);
  boite.pack_start(VT23);
  boite.pack_start(V12);
  boite.pack_start(V13);
  boite.pack_start(V32);
}

graphismeTableau::~graphismeTableau(){}

VBox& graphismeTableau::getBox(){return boite;}
