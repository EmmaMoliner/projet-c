#ifndef _GRAPHISMETABLEAU_HH_
#define _GRAPHISMETABLEAU_HH_

#include <gtkmm/hvbox.h>
#include <gtkmm/button.h>

using namespace Gtk;

class graphismeTableau{

private:
  VBox boite;
  Button P12;
  Button P22;
  Button P32;
  Button VT12;
  Button VT23;
  Button V12;
  Button V13;
  Button V32;
public:
  graphismeTableau();
  ~graphismeTableau();
  VBox& getBox();
};

#endif
