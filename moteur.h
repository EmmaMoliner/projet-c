#ifndef _MOTEUR_H
#define _MOTEUR_H

#include "reservoir.h"

class moteur:public element{

private:
  reservoir tank;
public :
  moteur();
  moteur(const int state,const string name,reservoir& res);
  moteur(moteur & m);
  ~moteur();
  reservoir& getReservoir();
  friend ostream& operator <<(ostream& flux, moteur & m);
};

#endif
