#include "pilote.hh"

/*Constructeur*/
pilote::pilote():nom(""),prenom(""),num_pilote(0),note_simulateur(0),histo(){}

pilote::pilote(const string nom, const string prenom, const int numero, const int note,historique& histo)
{
    this->nom = nom;
    this->prenom = prenom;
    num_pilote = numero;
    note_simulateur = note;
    this->histo = histo;
}

/*Destructeur*/
pilote::~pilote(){}

/*Getteur du nom*/
string pilote::getNom(){return nom;}

/*Getteur du prénom*/
string pilote::getPrenom(){return prenom;}

/*Getteur du numéro*/
int pilote::getNum(){return num_pilote;}

/*Getteur de la note*/
int pilote::getNote(){return note_simulateur;}

/*Getteur de l'historique*/
historique& pilote::getHisto(){return histo;}

/*Surcharge de l'opérateur <<*/
ostream& operator << (ostream& flux, pilote& p)
{
  flux <<"=========="<< p.nom << " " << p.prenom << "==========" << endl;
  flux << "Numéro de pilote : " << p.num_pilote << endl;
  flux << "Note : " << p.note_simulateur << endl;
  flux << "\n";
  flux << p.histo << endl;

  return flux;
}

/*Surcharge de l'opérateur =*/
pilote& pilote::operator = (pilote& p)
{
  nom = p.nom;
  prenom = p.prenom;
  num_pilote = p.num_pilote;
  note_simulateur = p.note_simulateur;
  histo = p.histo;

  return *this;
}
