#ifndef _PILOTE_HH_
#define _PILOTE_HH_

#include <string>
#include "historique.hh"

using namespace std;

class pilote
{
 private:
   string nom;
   string prenom;
   int num_pilote;
   int note_simulateur;
   historique histo;

 public:
   /*Constructeurs*/
   pilote();
   pilote(const string nom, const string prenom, const int numero, const int note,historique& histo);

   /*Destructeur*/
   ~pilote();

   /*Getteurs*/
   string getNom();
   string getPrenom();
   int getNum();
   int getNote();
   historique& getHisto();

   /*Surcharge de l'opérateur <<*/
   friend ostream& operator << (ostream& flux, pilote& p);

   /*Surcharge de l'opérateur =*/
   pilote& operator = (pilote& p);
};

#endif
