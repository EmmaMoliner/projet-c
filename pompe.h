#ifndef _POMPE_H
#define _POMPE_H

#include "element.h"

class pompe:public element{

public :
  pompe();
  pompe(const int state,const string name);
  pompe(pompe & p);
  ~pompe();
  friend ostream& operator <<(ostream& flux,pompe & p);

};

#endif
