#ifndef _RESERVOIR_H
#define _RESERVOIR_H

#include "pompe.h"

class reservoir:public element
{
private:
  pompe primaire;
  pompe secondaire;

public:
  reservoir();
  reservoir(const int state,const string name,pompe& prim,pompe& sec);
  reservoir(reservoir & r);
  ~reservoir();
  pompe& getPrimaire();
  pompe& getSecondaire();
  friend ostream& operator << (ostream& flux,reservoir& r);

};

#endif
