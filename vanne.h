#ifndef _VANNE_H
#define _VANNE_H

#include "element.h"

class vanne:public element
{
private:
  string type;
  element e1;
  element e2;

public:
  vanne();
  vanne(const int state,const string name,const string categorie,element& elem1,element& elem2);
  vanne(vanne & v);
  ~vanne();
  element& getElem1();
  element& getElem2();
  friend ostream& operator << (ostream& flux,vanne& v);

};

#endif
